# Weather app with vue.js

## About

For this project I was using vue.js, axios and OpenWeather API (https://openweathermap.org/).

Main focus was on building functionality to display current weather data 
with OpenWeather - Current weather data API (https://openweathermap.org/current).

## How to run

To run this project you just need to open app.html in your browser.

For this project to work you need to have Internet access, because some of the libraries are delivered via CDN and ofcourse OpenWeather API won't work without Internet.