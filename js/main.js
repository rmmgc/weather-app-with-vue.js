/*
 * Main JS file
 */


/*
 * Initialize new Vue instance
 */

var app = new Vue({

  el: '#app',

  data: {
    lat: 0.0,
    lng: 0.0,
    city: '',
    temperature: '',
    weather: {
      id: 0,
      description: ''
    },
    message: '',
    preloaderStatus: true
  },

  mounted: function () {

    // Call getGeolocation function when mounted hook is fired
    this.$nextTick(function () {
      this.getGeolocation();
    })

  },

  computed: {

    // Generate src for weather images
    iconSrc: function() {
      if (this.weather.id != 0){
        // Get image from weather API
        return 'http://openweathermap.org/img/wn/' + this.weather.icon  + '@2x.png';
      }
      else {
        // Default image
        return './images/unavailable.png';
      }
    }

  },

  methods: {

    // Geolocation functionality 
    getGeolocation: function() {
      if (!navigator.geolocation) {
        // If geolocation is not supported by browser show error message
        this.message = "Your browser does not support Geolocation";
        // Hide preloader element
        this.preloaderStatus  = false;

        return;
      }

      // Get current position
      navigator
        .geolocation
        .getCurrentPosition(this.geolocationSuccess, this.geolocationError, { timeout: 60000 });
    },

    // Run on success
    geolocationSuccess: function(data) {
      // Save coords data
      this.lat = data.coords.latitude;
      this.lng = data.coords.longitude;

      // Get weather data from API
      this.getWeatherData();
    },

    // Run on error
    geolocationError: function(err) {
      if(err.code === 1) {
        // Error message if user denied geolocation
        this.message = "You must enable geolocation to retrieve weather data.";
      }
      else {
        // Error message for every othen geolocation error
        this.message = "We are unable to retrieve your geolocation. Please, try again later.";
      }
      // Hide preloader element
      this.preloaderStatus  = false;
    },

    // Get data from weather API
    getWeatherData: function() {
      // Show preloader element until AJAX data is retrieved
      this.preloaderStatus  = true;

      // Generate correct API URL
      var API_URL = 'http://api.openweathermap.org/data/2.5/weather?lat=' + this.lat + 
                    '&lon=' + this.lng + '&APPID=69d3cf86b46f19cf3e049339355533aa';
  
      // Send AJAX request on generated URL
      axios.get(API_URL)
      .then((response) => {
        // Update data
        this.temperature      = (Number.parseFloat(response.data.main.temp) - 273.15).toFixed(1);
        this.city             = response.data.name;
        this.weather          = response.data.weather[0];
        this.message          = '';

        // Give some time to animation to finish
        setTimeout(() => {
          // Hide preloader element
          this.preloaderStatus  = false;
        }, 500);
      })
      .catch(function (error) {
        // Error message
        this.message = "We are unable to connect with weather API.";
        // Hide preloader element
        this.preloaderStatus  = false;
      });
    }

  }

})